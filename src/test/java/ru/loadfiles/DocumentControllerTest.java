package ru.loadfiles;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.loadfiles.dto.DocumentDto;
import ru.loadfiles.dto.DocumentsListDto;
import ru.loadfiles.dto.ResultDto;
import ru.loadfiles.services.DocumentService;

import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DocumentControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private DocumentService documentService;

    @Test
    public void shouldUploadAndDownloadFile() throws Exception {
        DocumentDto doc = restTemplate.postForObject("/docs", null, DocumentDto.class);
        assertNotNull(doc);

        ClassPathResource resource = new ClassPathResource("hello.txt", getClass());
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", resource);
        ResponseEntity<ResultDto> response = restTemplate.postForEntity("/docs/" + Long.toString(doc.id) + "/file", map, ResultDto.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);

        ResponseEntity<byte[]> responseFile  = restTemplate.getForEntity("/docs/" + Long.toString(doc.id) + "/file", byte[].class);
        assertEquals(responseFile.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseFile.getBody());
        String fileBody = new String(responseFile.getBody());
        assertTrue(fileBody.contains("Privet"));
    }

    @Test
    public void listDocuments() throws Exception {
        DocumentDto doc1 =  restTemplate.postForObject("/docs", null, DocumentDto.class);
        assertNotNull(doc1);
        DocumentDto doc2 =  restTemplate.postForObject("/docs", null, DocumentDto.class);
        assertNotNull(doc2);

        ResponseEntity<DocumentsListDto> response = restTemplate.getForEntity("/docs", DocumentsListDto.class);
        DocumentsListDto dto = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertTrue(dto.itemsCount >= 2L);
        assertTrue(dto.items.stream().map(a -> a.id).collect(Collectors.toList()).containsAll(Lists.newArrayList(doc1.id, doc2.id)));
    }
}
