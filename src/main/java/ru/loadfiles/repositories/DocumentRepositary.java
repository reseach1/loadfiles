package ru.loadfiles.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.loadfiles.models.Document;

import java.util.Optional;

public interface DocumentRepositary extends JpaRepository<Document, Long> {

    @Override
    Optional<Document> findById(Long aLong);

}
