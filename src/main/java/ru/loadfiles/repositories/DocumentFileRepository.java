package ru.loadfiles.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.loadfiles.models.Document;
import ru.loadfiles.models.DocumentFile;

import java.util.Optional;

public interface DocumentFileRepository extends JpaRepository<DocumentFile, Long> {

    @Override
    Optional<DocumentFile> findById(Long aLong);

    Optional<DocumentFile> findFirstByDocumentOrderByIdDesc(Document document);
}
