package ru.loadfiles.services;


import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;
import ru.loadfiles.exceptions.InternalServerError;
import ru.loadfiles.models.Document;

import java.util.List;

public interface DocumentService {

    Document createDocument();

    List<Document> getAllDocuments();

    void uploadDocumentFile(Long documentId, MultipartFile file) throws InternalServerError;

    FileSystemResource downloadDocumentFile(Long documentId) throws InternalServerError;

}
