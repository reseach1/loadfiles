package ru.loadfiles.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.loadfiles.exceptions.InternalServerError;
import ru.loadfiles.models.Document;
import ru.loadfiles.models.DocumentFile;
import ru.loadfiles.repositories.DocumentFileRepository;
import ru.loadfiles.repositories.DocumentRepositary;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Value("${loadfiles.dir:files}")
    private String rootLocation;
    private DocumentRepositary documentRepositary;
    private DocumentFileRepository documentFileRepository;

    @Autowired
    public void setDocumentRepositary(DocumentRepositary documentRepositary) {
        this.documentRepositary = documentRepositary;
    }

    @Autowired
    public void setDocumentFileRepository(DocumentFileRepository documentFileRepository) {
        this.documentFileRepository = documentFileRepository;
    }

    @Override
    @Transactional
    public Document createDocument() {
        Document document = new Document();
        documentRepositary.save(document);
        document.name = Long.toString(document.id) + " от " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yy"));
        return document;
    }

    @Override
    @Transactional
    public List<Document> getAllDocuments() {
        return documentRepositary.findAll();
    }

    @Override
    @Transactional
    public void uploadDocumentFile(Long documentId, MultipartFile file) throws InternalServerError {
        Optional<Document> document = documentRepositary.findById(documentId);
        if (!document.isPresent()) {
            return;
        }
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        DocumentFile documentFile = new DocumentFile();
        documentFile.document = document.get();
        documentFile.originalFileName = fileName;
        documentFileRepository.save(documentFile);

        if (!isDirectoryExists(rootLocation)) {
            Paths.get(rootLocation).toFile().mkdir();
        }
        Path fileLocation = Paths.get(rootLocation, Long.toString(documentFile.id) + "." + StringUtils.getFilenameExtension(fileName));
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, fileLocation,
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            documentFileRepository.delete(documentFile);
            throw new InternalServerError("Ошибка записи файла");
        }
    }

    @Override
    @Transactional
    public FileSystemResource downloadDocumentFile(Long documentId) throws InternalServerError {
        Optional<Document> document = documentRepositary.findById(documentId);
        if (!document.isPresent()) {
            throw new InternalServerError("Документ не найден");
        }
        Optional<DocumentFile> documentFile = documentFileRepository.findFirstByDocumentOrderByIdDesc(document.get());
        if (!documentFile.isPresent()) {
            throw new InternalServerError("Документ не найден");
        }
        DocumentFile file = documentFile.get();
        Path fileLocation = Paths.get(rootLocation, Long.toString(file.id) + "." + StringUtils.getFilenameExtension(file.originalFileName));
        return new FileSystemResource(fileLocation.toFile());
    }

    public static boolean isDirectoryExists(String directoryPath) {
        if (!Paths.get(directoryPath).toFile().isDirectory()) {
            return false;
        }
        return true;
    }

}
