package ru.loadfiles.models;

import javax.persistence.*;

@Entity
public class DocumentFile {

    @Id
    @GeneratedValue
    public Long id;

    @Column
    public String originalFileName;

    @ManyToOne
    public Document document;
}
