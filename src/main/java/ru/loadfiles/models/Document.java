package ru.loadfiles.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Document {

    @Id
    @GeneratedValue
    public Long id;

    /**
     * XX от DD.MM.YY”, где XX – порядковый номер договора, DD.MM.YY дата создания.
     */
    @Column
    public String name;

}
