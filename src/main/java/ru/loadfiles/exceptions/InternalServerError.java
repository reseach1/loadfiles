package ru.loadfiles.exceptions;

public class InternalServerError extends Exception {
    public InternalServerError() {
    }

    public InternalServerError(String message) {
        super(message);
    }
}
