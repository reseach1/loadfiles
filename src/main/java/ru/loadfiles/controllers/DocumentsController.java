package ru.loadfiles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.loadfiles.dto.DocumentDto;
import ru.loadfiles.dto.DocumentsListDto;
import ru.loadfiles.dto.ResultDto;
import ru.loadfiles.exceptions.InternalServerError;
import ru.loadfiles.services.DocumentService;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/docs")
public class DocumentsController {

    private DocumentService documentService;

    @Autowired
    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    DocumentsListDto list() throws IOException {
        List<DocumentDto> list = documentService.getAllDocuments().stream().map(DocumentDto::new).collect(Collectors.toList());
        DocumentsListDto dto = new DocumentsListDto();
        dto.items = list;
        dto.itemsCount = Long.valueOf(list.size());
        return dto;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    DocumentDto createDocument() {
        return new DocumentDto(documentService.createDocument());
    }

    @RequestMapping(value = "/{id}/file", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody
    FileSystemResource downloadDocumentFile(@PathVariable Long id) throws InternalServerError {
        return documentService.downloadDocumentFile(id);
    }

    @RequestMapping(value = "/{id}/file", method = RequestMethod.POST)
    public @ResponseBody
    ResultDto uploadDocumentFile(@PathVariable Long id, @RequestParam("file") MultipartFile file) throws InternalServerError {
        documentService.uploadDocumentFile(id, file);
        return new ResultDto("OK");
    }
}
