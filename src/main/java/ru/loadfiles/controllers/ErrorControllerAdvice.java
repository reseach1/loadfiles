package ru.loadfiles.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.loadfiles.dto.ResultDto;
import ru.loadfiles.exceptions.InternalServerError;

@ControllerAdvice
public class ErrorControllerAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResultDto internalErrorHandler(Exception exception) {
        return new ResultDto(exception.getLocalizedMessage());
    }

    @ExceptionHandler(InternalServerError.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResultDto internalErrorHandler(InternalServerError exception) {
        return new ResultDto(exception.getLocalizedMessage());
    }
}
