package ru.loadfiles.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ResultDto {
    public String error;

    public ResultDto(String error) {
        this.error = error;
    }
}
