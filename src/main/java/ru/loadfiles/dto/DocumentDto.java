package ru.loadfiles.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import ru.loadfiles.models.Document;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDto {

    public Long id;

    public String name;

    public DocumentDto(Document document) {
        this.id = document.id;
        this.name = document.name;
    }
}
