package ru.loadfiles.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentsListDto {
	public Long itemsCount;
	public List<DocumentDto> items;
}
